//
//  main.m
//  VLCPlay
//
//  Created by Marvin Scholz on 20.12.19.
//  Copyright © 2019 ePirat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
