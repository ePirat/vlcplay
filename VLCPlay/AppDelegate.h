//
//  AppDelegate.h
//  VLCPlay
//
//  Created by Marvin Scholz on 20.12.19.
//  Copyright © 2019 ePirat. All rights reserved.
//
#include <vlc/vlc.h>

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate> {
    @public
    libvlc_instance_t *_libvlc;
    libvlc_media_player_t *_vlc_player;
}

@property (strong, nonatomic) UIWindow *window;


@end

