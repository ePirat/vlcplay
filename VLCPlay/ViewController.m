//
//  ViewController.m
//  VLCPlay
//
//  Created by Marvin Scholz on 20.12.19.
//  Copyright © 2019 ePirat. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"

@interface ViewController ()
@property (nonatomic, strong) IBOutlet UIView *videoView;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    AppDelegate *d = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSLog(@"Setting view to %@", _videoView);
    libvlc_media_player_set_nsobject(d->_vlc_player, (__bridge_retained void*)_videoView);

    // Play
    libvlc_media_player_play(d->_vlc_player);
}


@end
